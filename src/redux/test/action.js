import actionType from 'Redux/test/actionType';

export default {
  test: (data) => ({type: actionType.TEST, data}),
  testRequest: () => ({type: actionType.TEST_REQUEST}),
  testSuccess: (data) => ({type: actionType.TEST_SUCCESS, data}),
  testFailure: (data) => ({type: actionType.TEST_FAILURE, data}),
};
