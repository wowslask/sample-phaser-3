import {put, delay} from 'redux-saga/effects';

import TestAction from 'Redux/test/action';

import {Api} from 'Utils/index';

export function* testApi() {
  yield put(TestAction.testRequest());

  try {
    yield delay(4000)
    const {data} = yield Api.instance.get(Api.routes.node.index);

    yield put(TestAction.testSuccess(data))
  } catch (error) {
    yield put(TestAction.testFailure(error));
  }
};
