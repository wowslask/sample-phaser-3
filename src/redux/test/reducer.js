import actionType from 'Redux/test/actionType';

const initialState = {
  test: '',
};

export default {
  initialState,
  reducer: (state = initialState, action) => {
    switch (action.type) {
      case actionType.TEST_SUCCESS:
        return {...state, test: action.data}
      default:
        return state;
    };
  },
};
