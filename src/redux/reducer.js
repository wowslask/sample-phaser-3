import {combineReducers} from 'redux';

import TestReducer from 'Redux/test/reducer';
import LoadingReducer from 'Redux/loading/reducer';
import ErrorReducer from 'Redux/error/reducer';

const initialState = {
  test: TestReducer.initialState,
  loading: LoadingReducer.initialState,
  error: ErrorReducer.initialState,
};

const reducer = combineReducers({
  test: TestReducer.reducer,
  loading: LoadingReducer.reducer,
  error: ErrorReducer.reducer,
});

export default {
  initialState,
  reducer,
};
