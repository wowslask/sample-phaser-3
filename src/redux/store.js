import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import Reducers from 'Redux/reducer';
import Saga from 'Redux/saga';

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV === 'development') {
    const composeEnhancers = composeWithDevTools({trace: true});
    return composeEnhancers(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

export default function initializeStore(initialState = Reducers.initialState) {
  const sagaMiddleware = createSagaMiddleware();
  
  const store = createStore(
    Reducers.reducer,
    initialState,
    bindMiddleware([sagaMiddleware]),
  );

  store.sagaTask = sagaMiddleware.run(Saga);

  return store;
};
