import {all, takeLatest} from 'redux-saga/effects';

import TestActionType from 'Redux/test/actionType';
import * as TestSaga from 'Redux/test/saga';

const test = [
  takeLatest(TestActionType.TEST, TestSaga.testApi),
];

export default function* rootSaga() {
  yield all([
    ...test,
  ]);
};
