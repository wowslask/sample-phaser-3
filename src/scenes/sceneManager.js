import {Scene, Constants, Grid} from 'Utils/index';

import DoraemonImage from 'Assets/spritesheets/doraemon.png';
import JoeImage from 'Assets/spritesheets/walking.png';

class SceneManager extends Scene {
  constructor() {
    super({key: Constants.scenes.sceneManager});
  };

  async preload() {
    this.load.spritesheet(Constants.spritesheet.character.doraemon, DoraemonImage, {frameWidth: 220, frameHeight: 320});
    this.load.spritesheet(Constants.spritesheet.character.joe, JoeImage, {frameWidth: 150, frameHeight: 310});
  };

  create() {
    const grid = new Grid(this, 11, 11);
    // grid.show();
    // grid.showNumbers();
    
    this.doraemon = this.add.sprite(0, 0, Constants.spritesheet.character.doraemon)
    this.anims.create({
      key: Constants.animations.character.doraemon.running,
      frameRate: 10,
      frames: this.anims.generateFrameNumbers(Constants.spritesheet.character.doraemon),
      repeat: -1
    });
    this.utils.align.center(this.doraemon)
    this.utils.align.scaleToGameWidth(this.doraemon, .1)
    this.doraemon.play(Constants.animations.character.doraemon.running)

    this.joe = this.add.sprite(0, 0, Constants.spritesheet.character.joe);
    this.anims.create({
      key: Constants.animations.character.joe.walking,
      frameRate: 5,
      frames: this.anims.generateFrameNumbers(Constants.spritesheet.character.joe),
      repeat: -1,
    });
    grid.placeAtCell(25, this.joe);
    this.utils.align.scaleToGameWidth(this.joe, .5)
    this.joe.play(Constants.animations.character.joe.walking)

    this.joe1 = this.add.sprite(0, 0, Constants.spritesheet.character.joe);
    this.anims.create({
      key: `${Constants.animations.character.joe.walking}1`,
      frameRate: 5,
      frames: this.anims.generateFrameNumbers(Constants.spritesheet.character.joe),
      repeat: -1,
    });
    grid.placeAtCell(85, this.joe1);
    this.joe1.play(`${Constants.animations.character.joe.walking}1`);
  };
};

export default SceneManager;
