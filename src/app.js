import Phaser from 'phaser';

import 'Css/canvas.scss';

import {SceneManager} from 'Scenes/index';

const canvasContainer = document.getElementById('avatar-canvas-container');

console.log(canvasContainer.offsetWidth, canvasContainer.offsetHeight)
new Phaser.Game({
  width: canvasContainer.offsetWidth,
  height: canvasContainer.offsetHeight,
  backgroundColor: 0xD5D5D5,
  scene: [SceneManager],
  pixelArt: true,
  mode: Phaser.Scale.FIT,
  autoCenter: Phaser.Scale.CENTER_BOTH,
  parent: 'avatar-canvas-container'
});
