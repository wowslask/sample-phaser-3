let instance = null;

import {Store} from 'redux';

import CreateStore from 'Redux/store';

class ReduxStore {
  constructor() {
    if (!instance) {
      instance = CreateStore();
    }
  };

  /**
   * Initiate Redux Store
   * @returns {Store} redux store
   */
  static getInstance() {
    if (!instance) {
      new ReduxStore();
    }

    return instance;
  };
};

export default ReduxStore;
