import Phaser from 'phaser';

class Grid {
  /**
   * @param {Phaser.Scene} context Scene context
   * @param {number} rows Number of rows in the grid
   * @param {number} cols Number of cols in the grid
   * @param {?number} width cell width
   * @param {?number} height cell height
   */
  constructor(context, rows, cols, width, height) {
    this.context = context;

    this.rows = rows;
    this.cols = cols;
    this.width = width || this.context.game.config.width;
    this.height = height || this.context.game.config.height;

    this.cellWidth = this.width / this.cols;
    this.cellHeight = this.height / this.rows;
  };

  /**
   * show the cell grid
   */
  show() {
    const cellGraphic = this.context.add.graphics();
    cellGraphic.lineStyle(2, 0xff0000);

    for (let i = 0; i < this.width; i += this.cellWidth) {
      cellGraphic.moveTo(i, 0);
      cellGraphic.lineTo(i, this.height);
    }

    for (let i = 0; i < this.height; i += this.cellHeight) {
      cellGraphic.moveTo(0, i);
      cellGraphic.lineTo(this.width, i);
    };

    cellGraphic.strokePath();
  };

  /**
   * show the cell number
   */
  showNumbers() {
    let count = 0;
    for (let rowCount = 0; rowCount < this.rows; rowCount++) {
      for (let colCount = 0; colCount < this.cols; colCount++) {
        const cellNumber = this.context.add.text(0, 0, count, {color: '#ff0000', font: `${(this.cellWidth * this.cellHeight) * .01}px`})
        cellNumber.setOrigin(0.5, 0.5);
        this.placeAtCell(count, cellNumber);
        count++;
      }
    }
  };

  /**
   * place game object at designated cell
   * @param {number} cell cell position
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   */
  placeAtCell(cell, object) {
    const y = Math.floor(cell / this.cols);
    const x = cell - (y * this.cols);

    this.placeAtAxis(x, y, object);
  }

  /**
   * place game object at designated position
   * @param {number} x The horizontal position of this Game Object in the world.
   * @param {number} y The vertical position of this Game Object in the world.
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   */
  placeAtAxis(x, y, object) {
    object.x = (this.cellWidth * x) + (this.cellWidth / 2);
    object.y = (this.cellHeight * y) + (this.cellHeight / 2);
  }
};

export default Grid;
