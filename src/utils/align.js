import Phaser from 'phaser';

class Align {
  /**
   * @param {Phaser.Scene} context Scene context
   */
  constructor(context) {
    this.context = context;
  };

  /**
   * Game object  will be scaled to game width by percentage
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   * @param {number} percentage percentage value of game width
   */
  scaleToGameWidth(object, percentage) {
    object.displayWidth = this.context.game.config.width * percentage;
    object.scaleY = object.scaleX;
  }

  /**
   * Game object  will be center in game x axis
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   */
  centerX (object) {
    object.x = this.context.game.config.width / 2;
  };
  
  /**
   * Game object will be center in game y axis
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   */
  centerY (object) {
    object.y = this.context.game.config.height / 2;
  };

  /**
   * Game Object will be center in game x and y
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   */
  center(object) {
    this.centerX(object);
    this.centerY(object);
  };
};

export default Align;
