import axios from 'axios';

export default {
  instance: axios.create({
    baseURL: process.env.API_URL,
  }),
  routes: {
    node: {
      index: 'node'
    },
  },
};
