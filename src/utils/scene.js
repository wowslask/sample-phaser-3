import Phaser from 'phaser';

import {ReduxStore, Align} from 'Utils/index';

class Scene extends Phaser.Scene {
  /**
   * @param {Phaser.Types.Scenes.SettingsConfig} settingConfig Scene specific configuration settings.
   * @param {object} config
   * @param {boolean} config.redux Scene will use redux store
   */
  constructor(settingConfig, config = {}) {    
    super(settingConfig);

    this.utils = {
      align: new Align(this),
    };

    const {redux} = config;

    if (redux) {
      this.store = ReduxStore.getInstance();
      this.state = {...this.store.getState()};

      this.storeUnsubscribe = this.store.subscribe(() => {
        this.state = {...this.state, ...this.store.getState()};
      });
    }
  }
};

export default Scene;
