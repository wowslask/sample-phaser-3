import Phaser from 'phaser';

class PathFollower {
  /**
   * @param {Phaser.GameObjects.Sprite} object GameObject Sprite
   * @param {object} config
   * @param {Phaser.Curves.Path} config.path path the game object follow
   * @param {number} t time duration
   */
  constructor(object, {path, t}) {
    this.object = object;
    this.path = path;
    this.pathVector = undefined;
    this.t = t;
    this._t = 0;
  };

  get t() {
    return this._t;
}

  set t(value) {
      this._t = value;
      this.update();
  }

  update() {
    this.pathVector = this.path.getPoint(this.t, this.pathVector);
    this.object.setPosition(this.pathVector.x, this.pathVector.y);
  };
};

export default PathFollower;
