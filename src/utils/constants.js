export default {
  scenes: {
    sceneManager: 'Scene Manager'
  },
  spritesheet: {
    character: {
      doraemon: 'Doraemon',
      joe: 'Joe'
    },
  },
  animations: {
    character: {
      doraemon: {
        running: 'Doraemon Running Animation',
      },
      joe: {
        walking: 'Joe Walking Animation',
      },
    },
  },
};
